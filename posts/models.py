from django.db import models

# Create your models here.

class Post(models.Model): # new
  title = models.CharField(max_length=50)
  text = models.TextField()

  def __str__(self): # new
    return self.title
  